import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn import metrics 
from sklearn.tree import export_graphviz
from sklearn.externals.six import StringIO  
from IPython.display import Image  
import pydotplus
import matplotlib.pyplot as plt

col_names = ['Cement', 'Blast Furnace Slag',
       'Fly Ash', 'Water',
       'Superplasticizer',
       'Coarse Aggregate',
       'Fine Aggregate', 'Age(day)',
       'Concrete compressive strength(Mpa)','CategoricalOutcome']

df= pd.read_excel("Concrete_Data.xls", names=col_names)

# Divide columns in features and target variable
feat_cols = ['Cement', 'Blast Furnace Slag',
       'Fly Ash', 'Water',
       'Superplasticizer',
       'Coarse Aggregate',
       'Fine Aggregate', 'Age(day)']

X = df[feat_cols] 
y = df.CategoricalOutcome


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=1) # 75% training and 25% test

# Create decision tree classifer 
dtc = DecisionTreeClassifier(max_depth = 3)   # threshold on the maximum tree depth of 3

# Train decision tree classifer
dtc = dtc.fit(X_train,y_train)

#Predict the response 
y_pred = dtc.predict(X_test)
y_proba= dtc.predict_proba(X_test)


# Model specificity
print(metrics.classification_report(y_test, y_pred))
print( "\n" , metrics.confusion_matrix(y_test, y_pred))

fpr, tpr, thresholds = metrics.roc_curve(y_test, y_proba[:,0], pos_label=dtc.classes_[0], drop_intermediate=False)
plt.plot(fpr, tpr, color='darkorange')
plt.title('Receiver Operating Characteristic curve')
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')

# Plot decision tree 
dot_data = StringIO()
export_graphviz(dtc, out_file=dot_data,  
                filled=True, rounded=True,
                special_characters=True,feature_names = feat_cols)

graph = pydotplus.graph_from_dot_data(dot_data.getvalue())  
graph.write_png('Concrete_tree.png')
Image(graph.create_png())
