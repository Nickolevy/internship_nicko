import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from imblearn.over_sampling import RandomOverSampler
from sklearn.model_selection import train_test_split
from sklearn import metrics 
from sklearn.tree import export_graphviz
from sklearn.externals.six import StringIO  
from IPython.display import Image  
import pydotplus


df= pd.read_excel("Concrete_Ratio.xls")

# Divide columns in features and target variable
feat_cols = ['Cement(kg in a m3 mixture)', 'Blast Furnace Slag(kg in a m3 mixture)',
       'Fly Ash(kg in a m3 mixture)', 'Water(kg in a m3 mixture)',
      'Superplasticizer(kg in a m3 mixture)',
      'Coarse Aggregate(kg in a m3 mixture)',
       'Fine Aggregate(kg in a m3 mixture)', 'Age(day)',
       'Water to cement ratio', 'Binder', 'Water to binder',
       'Superplasticizer to binder', 'Fly Ash to binder',
       'Blast Furnace Slag to binder', 'Fly Ash + Slag to binder']  

X = df[feat_cols] 
y = df.CategoricalOutcome

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=1) # 75% training and 25% test

#Balanced Data 
ros = RandomOverSampler(random_state=0)
X_train, y_train = ros.fit_sample(X_train, y_train)

# Create decision tree classifer 
dtc = DecisionTreeClassifier(max_depth = 3)   # threshold on the maximum tree depth of 3

# Train decision tree classifer
dtc = dtc.fit(X_train,y_train)

#Predict the response 
y_pred = dtc.predict(X_test)
y_proba= dtc.predict_proba(X_test)


# Model specificity
print(metrics.confusion_matrix(y_test, y_pred))
print( "\n" , metrics.classification_report(y_test, y_pred))

# Plot decision tree 
dot_data = StringIO()
export_graphviz(dtc, out_file=dot_data,  
                filled=True, rounded=True,
                special_characters=True,feature_names = feat_cols)

graph = pydotplus.graph_from_dot_data(dot_data.getvalue())  
graph.write_png('Concrete_tree.png')
Image(graph.create_png())

