import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score ,mean_absolute_error

df= pd.read_excel("Concrete_Data.xls")

X = df.iloc[:,:-2] # Feat_col 
y = df.iloc[:,-2] # Target 
 
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2,random_state=1)  # 80% training and 20% test

# Create LinearRegression
lr = LinearRegression() 

# Train Linear Regression
lr.fit(X_train, y_train)

#Predict the response 
y_pred = lr.predict(X_test) 

print("\t\t\t RMSE\t MSE\t R2\t MAE") 
print("""Linear Regression \t {:.2f}\t{:.2f}\t{:.2f}\t{:.2f}""".format(np.sqrt(mean_squared_error(y_test, y_pred)),
      mean_squared_error(y_test, y_pred),r2_score(y_test, y_pred),mean_absolute_error(y_test, y_pred)))