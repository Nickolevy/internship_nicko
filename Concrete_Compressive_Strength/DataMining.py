# Libraries
import pandas as pd 
import matplotlib.pyplot as plt
import seaborn as sns

sns.set_style("whitegrid")

# Import csv file.
col_names = ['Cement', 'Blast Furnace Slag',
       'Fly Ash', 'Water',
       'Superplasticizer',
       'Coarse Aggregate',
       'Fine Aggregate', 'Age(day)',
       'Concrete compressive strength(Mpa)','CategoricalOutcome']

df= pd.read_excel("Concrete_Data.xls", names=col_names)

# Distribution of each columns.
plt.rcParams.update({'font.size': 12})
fig,ax = plt.subplots(figsize = (15,8))

df.hist(ax=ax)

plt.tight_layout()
plt.savefig('Distributions.pdf')

# Categorical outcome
Low_Strength = df.query('CategoricalOutcome== 0')
Moderate_Strength = df.query('CategoricalOutcome== 1')
High_Strength = df.query('CategoricalOutcome== 2')

fig, axes = plt.subplots(2,5,figsize=(20,10))
i = j = 0

for col in Low_Strength.columns:    

    ax = axes[i//5][j % 5]
      
    sns.distplot(High_Strength[col], label ='H', ax = ax, color = 'green')  
    sns.distplot(Moderate_Strength[col], label ='M', ax = ax, color = 'yellow')  
    sns.distplot(Low_Strength[col], label ='L', ax = ax, color = 'red')
    
    ax.legend()   
    i = (i + 1)    
    j = (j + 1) 
    plt.tight_layout()
    
plt.savefig('Distributions_per_class.pdf')

# Pearson correlation
feat_cols = ['Cement', 'Blast Furnace Slag',
       'Fly Ash', 'Water',
       'Superplasticizer',
       'Coarse Aggregate',
       'Fine Aggregate', 'Age(day)','Concrete compressive strength(Mpa)' ]


corr = df[feat_cols].corr() 
plt.figure(figsize=(10,5))
sns.heatmap(corr, annot=True, cmap='coolwarm')
plt.tight_layout()
plt.savefig('Heatmap.pdf')
