# Libraries
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from keras.models import Sequential
from keras.layers.normalization import BatchNormalization
from keras.layers import Dense
import keras as ks 
import matplotlib.pyplot as plt

# Importing the dataset
df= pd.read_excel("Concrete_Data.xls")


feat_cols = ['Cement(kg in a m3 mixture)', 'Blast Furnace Slag(kg in a m3 mixture)',
       'Fly Ash(kg in a m3 mixture)', 'Water(kg in a m3 mixture)',
      'Superplasticizer(kg in a m3 mixture)',
      'Coarse Aggregate(kg in a m3 mixture)',
       'Fine Aggregate(kg in a m3 mixture)', 'Age(day)']   

X = df[feat_cols] 
y = df.CategoricalOutcome

X_train, X_test, y_train, y_test = train_test_split(X ,y , test_size=0.20)

# Feature Scaling
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)


# Building  first layer 
model=Sequential()
model.add(Dense(16,input_dim=8,activation = 'relu'))
# Building second hidden layer
model.add(Dense(8,activation='relu'))
model.add(BatchNormalization())
# Output layer
model.add(Dense(1,activation='sigmoid'))

# Optimize the model 
opt =ks.optimizers.Adam(lr=0.01)  #learning rate

# Compile the model 
model.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])
# Train the model
history = model.fit(X_train,y_train,batch_size=5,epochs = 100,validation_split=0.1)

#Results
_,accuracy = model.evaluate(X_test, y_test, verbose=0)
print('Accuracy: %.2f' % (accuracy*100))

#Plot history
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
