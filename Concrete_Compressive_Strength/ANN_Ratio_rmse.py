# Libraries
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from keras.models import Sequential
from keras.layers.normalization import BatchNormalization
from keras.layers import Dense
from keras import backend
import keras as ks 
from sklearn.metrics import r2_score

# Importing the dataset
df= pd.read_excel("Concrete_Ratio.xls")

feat_cols = ['Cement(kg in a m3 mixture)', 'Blast Furnace Slag(kg in a m3 mixture)',
       'Fly Ash(kg in a m3 mixture)', 'Water(kg in a m3 mixture)',
      'Superplasticizer(kg in a m3 mixture)',
      'Coarse Aggregate(kg in a m3 mixture)',
       'Fine Aggregate(kg in a m3 mixture)', 'Age(day)',
       'Water to cement ratio', 'Binder', 'Water to binder',
       'Superplasticizer to binder', 'Fly Ash to binder',
      'Blast Furnace Slag to binder', 'Fly Ash + Slag to binder']   #With Ratio


X = df[feat_cols] 
y = df.CS

X_train, X_test, y_train, y_test = train_test_split(X ,y , test_size=0.20)

# Feature Scaling
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

# The mean square error : metric function
def rmse(y_true, y_pred):
	return backend.sqrt(backend.mean(backend.square(y_pred - y_true), axis=-1))

# Building  first layer 
model=Sequential()
model.add(Dense(64,input_dim=15,activation = 'relu'))
# Building second hidden layer
model.add(Dense(32,activation='relu'))
model.add(BatchNormalization())
# Output layer
model.add(Dense(1,activation='linear'))

# Optimize the model 
opt =ks.optimizers.Adam(lr=0.1)  #learning rate 
# Compile the model 
model.compile(optimizer=opt,loss='mean_squared_error',metrics=[rmse])
# Train the model
history = model.fit(X_train,y_train,batch_size=50,epochs = 100,validation_split=0.1)

# Predicting and R² score
y_predict = model.predict(X_test)
print('R2 score = ', r2_score(y_test,y_predict))



