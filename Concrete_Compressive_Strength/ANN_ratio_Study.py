# Libraries
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import SGD
from plot_keras_history import plot_history
import matplotlib.pyplot as plt 

# Importing the dataset
df= pd.read_excel("Concrete_Class_ratio.xls")

feat_cols = ['Cement(kg in a m3 mixture)', 'Blast Furnace Slag(kg in a m3 mixture)',
       'Fly Ash(kg in a m3 mixture)', 'Water(kg in a m3 mixture)',
      'Superplasticizer(kg in a m3 mixture)',
      'Coarse Aggregate(kg in a m3 mixture)',
       'Fine Aggregate(kg in a m3 mixture)', 'Age(day)','Water to binder ratio']   

X = df[feat_cols] 
y = df.CategoricalOutcome

X_train, X_test, y_train, y_test = train_test_split(X ,y, test_size=0.50)

# Feature Scaling
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

# Building  first layer 
model=Sequential()
model.add(Dense(9,input_dim=9,activation = 'relu'))
# Building second hidden layer
model.add(Dense(6,activation='relu'))
# Output layer
model.add(Dense(3,activation='sigmoid'))

# Optimize the model 
opt= SGD(lr=0.0091, momentum=0.79) 

# Compile the model 
model.compile(optimizer = 'sgd', loss = 'sparse_categorical_crossentropy', metrics = ['accuracy'])
# Train the model
history = model.fit(X_train,y_train,batch_size=100, validation_split = 0.1, epochs=100)
plot_history(history.history)
plt.show()
# Results 
_,accuracy= model.evaluate(X_test,y_test)
print('Accuracy of testing: %.2f' % (accuracy*100))

_,accuracy1= model.evaluate(X_train,y_train)
print('Accuracy of training: %.2f' % (accuracy1*100))




