import pandas as pd
from imblearn.over_sampling import RandomOverSampler
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import classification_report, confusion_matrix

col_names = ['Cement', 'Blast Furnace Slag',
       'Fly Ash', 'Water',
       'Superplasticizer',
       'Coarse Aggregate',
       'Fine Aggregate', 'Age(day)',
       'Concrete compressive strength(Mpa)','CategoricalOutcome']

df= pd.read_excel("Concrete_Data.xls", names=col_names)

# Divide columns in features and target variable
feat_cols = ['Cement', 'Blast Furnace Slag',
       'Fly Ash', 'Water',
       'Superplasticizer',
       'Coarse Aggregate',
       'Fine Aggregate', 'Age(day)']

X = df[feat_cols] 
y = df.CategoricalOutcome


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=1)


ros = RandomOverSampler(random_state=0)
X_train, y_train = ros.fit_sample(X_train, y_train)

dtc = DecisionTreeClassifier(max_depth = 3) 
dtc = dtc.fit(X_train,y_train)
y_pred = dtc.predict(X_test)

print("\n""\t""RandomOverSampler""\n")
print(confusion_matrix(y_test, y_pred))
print("\n" ,classification_report(y_test, y_pred))

