import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score ,mean_absolute_error

df= pd.read_excel("Concrete_Ratio.xls")

feat_cols = ['Cement(kg in a m3 mixture)', 'Blast Furnace Slag(kg in a m3 mixture)',
       'Fly Ash(kg in a m3 mixture)', 'Water(kg in a m3 mixture)',
       'Superplasticizer(kg in a m3 mixture)',
       'Coarse Aggregate(kg in a m3 mixture)',
       'Fine Aggregate(kg in a m3 mixture)', 'Age(day)',
       'Water to cement ratio', 'Binder', 'Water to binder',
       'Superplasticizer to binder', 'Fly Ash to binder',
       'Blast Furnace Slag to binder', 'Fly Ash + Slag to binder']

X = df[feat_cols] 
y = df.CS

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25,random_state=1)  

# Create LinearRegression
lr = LinearRegression() 

# Train Linear Regression
lr.fit(X_train, y_train)

#Predict the response 
y_pred = lr.predict(X_test) 

print("\t\t\t RMSE\t MSE\t R2\t MAE") 
print("""Linear Regression \t {:.2f}\t{:.2f}\t{:.2f}\t{:.2f}""".format(np.sqrt(mean_squared_error(y_test, y_pred)),
      mean_squared_error(y_test, y_pred),r2_score(y_test, y_pred),mean_absolute_error(y_test, y_pred)))
